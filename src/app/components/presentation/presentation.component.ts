import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})
export class PresentationComponent implements OnInit {
  public skills;
  public range;
  constructor() { }

  ngOnInit() {
    this.range = (value) => {
      let a = [];
      for(let i = 0; i < value; ++i) { a.push(i+1) } return a;
    };
    this.skills = [
      {
        title: 'PHP',
        description: 'Advanced use of PHP Object. Experienced with: eZPublish, Symfony, Wordpress',
        level: 5,
      },
      {
        title: 'Angular',
        description: 'Experienced in AngularJS and starting Angular 2',
        level: 4,
      },
      {
        title: 'Typescript',
        description: 'Use of typescript to provide cleaner code for AngularJS',
        level: 3,
      }
    ];
  }

}
