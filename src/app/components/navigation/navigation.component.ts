import { Component, OnInit } from '@angular/core';
import { FakedataService } from '../../services/fakedata.service';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
    public data: any;

    constructor(public dataService: FakedataService) { }

    ngOnInit() {
        this.data = {
            socialLinks: this.dataService.socialLinks,
            navigationList: this.dataService.navigationList
        };
    }

}
