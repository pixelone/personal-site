import { Component, OnInit } from '@angular/core';
import { FakedataService } from '../../services/fakedata.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor(public FakedataService: FakedataService) { }

  ngOnInit() {}

}
