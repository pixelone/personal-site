import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { FakedataService } from '../../services/fakedata.service';

@Component({
    selector: 'app-viewproject',
    templateUrl: './viewproject.component.html',
    styleUrls: ['./viewproject.component.scss']
})
export class ViewprojectComponent implements OnInit {
    public currentProject: string;
    public projectHTMLContent: string;
    constructor( private route: ActivatedRoute, private router: Router, private fakeData: FakedataService, private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.route.params.subscribe(
            data => this.initProject(data['name']),
            err => console.log(err)
        );
    }

    private initProject(projectName: string){
        this.currentProject = projectName;
        this.fakeData.getProject(projectName).subscribe(
            data => this.projectHTMLContent = data._body,
            err => console.log(err)
        );
    }

}
