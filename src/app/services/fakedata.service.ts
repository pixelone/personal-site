import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable} from 'rxjs/Rx';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class FakedataService {

    public navigationList = [
        {name: 'Presentation', url: '/presentation'},
        {name: 'Projects', url: '/projects'},
        {name: 'Technology', url: '/technology'},
        {name: 'CV', url: '/cv'}
    ];

    public socialLinks = [
        {name: '<i class="fa fa-linkedin"></i>', url: 'https://fr.linkedin.com/in/robertfabien'}
    ];

    constructor(private http: Http) { }

    public getProject(projectFileName: string){
        return this.http.get(`/assets/projects/${projectFileName.toLowerCase()}.html`)
            .map((res:Response) => {
                return res
            })
            .catch((error:any) => Observable.throw(error.json().error || 'Server Error'));
    }
}
