/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FakedataService } from './fakedata.service';

describe('Service: Fakedata', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [FakedataService]
        });
    });

    it('should ...', inject([FakedataService], (service: FakedataService) => {
        expect(service).toBeTruthy();
    }));
});
