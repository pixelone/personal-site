import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { RouterModule } from '@angular/router';

import { FakedataService } from './services/fakedata.service';

import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { IndexComponent } from './components/index/index.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { TechnologyComponent } from './components/technology/technology.component';
import { PresentationComponent } from './components/presentation/presentation.component';
import { ViewprojectComponent } from './components/viewproject/viewproject.component';
import { CvComponent } from './components/cv/cv.component';
import { SafeHtmlPipe } from './saf-html.pipe';

let Routes = [
    {path: '', component: IndexComponent},
    {path: 'presentation', component: PresentationComponent},
    {path: 'projects', component: ProjectsComponent},
    {path: 'projects/:name', component: ViewprojectComponent},
    {path: 'technology', component: TechnologyComponent},
    {path: 'cv', component: CvComponent}
];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MaterialModule.forRoot(),
        RouterModule.forRoot(Routes)
    ],
    declarations: [
        AppComponent,
        NavigationComponent,
        IndexComponent,
        ProjectsComponent,
        TechnologyComponent,
        PresentationComponent,
        ViewprojectComponent,
        CvComponent,
        SafeHtmlPipe
    ],
    providers: [FakedataService],
    bootstrap: [AppComponent]
})
export class AppModule { }
